Simple SQL Batch Runner
============

Command-line SQL script runner that can run a batch of `.sql` files.

Why ?
-----------------
The idea came from my current work (late-2017).
We often need to run a batch of SQL scripts on multiple databases on a Microsoft&reg; SQL Server&trade;.

We had a batch file solution that required fiddling with configuration files just to indicate which database(s) to run on. I was tired of having to check the files at every run. The script also didn't go through folders recursively, another hitch to scratch !

What ?
------
Simple SQL batch runner that detects all `.sql` files in a folder and subfolders and runs them in alphabetical order.

You can specify multiple databases to run the scripts against.

How to install
--------------

For now, there's no installation package.
Clone the code and compile it. You need .NET Core 2.0 and C# 7.
All Nuget packages should fetch automatically.

How to run
----------

    dotnet RunSQL.dll -s|--server server -d|--database database1,database2 [-f base-scripts-folder] [-u|--user username] [-p|--pwd password] [-v|--v] [--vv]

    -d, --database    List of databases (separated with colon -> :) to apply scripts.

    -f, --folder      Full path to directory containing SQL script files (*.sql).

    -p, --pwd         Password for username. Ignored if username is not provided.

    -s, --server      Required. SQL server connection string.

    -u, --user        Username for SQL Server. If not provided, uses Windows Authentication.

    -v, --v           Verbose output (Debug level).

    --vv              Very verbose output (Trace level).

    --help            Display this help screen.
