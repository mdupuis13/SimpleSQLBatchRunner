﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Extensions.Logging;

namespace RunSQL.BatchRunners
{
    public class DOSCommandRunner
    {
        private ILogger logger;
        private bool _cmdInError;

        public DOSCommandRunner(ILogger<DOSCommandRunner> logger)
        {
            this.logger = logger;
        }

        // this class' code taken from https://stackoverflow.com/a/29753402
        public void RunCommand(string command, string args)
        {
            _cmdInError = false;

            //* Create your Process
            Process process = new Process();
            process.StartInfo.FileName = command;
            process.StartInfo.Arguments = args;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            //* Set your output and error (asynchronous) handlers
            process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);
            process.ErrorDataReceived += new DataReceivedEventHandler(ErrorOutputHandler);
            
            //* Start process and handlers
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();

            if (_cmdInError)
                throw new SubCommandException("Sub command error");
        }

        private void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            this.logger.LogDebug($"{outLine.Data}");
        }

        private void ErrorOutputHandler(object sendingPRocess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            this.logger.LogError($"{outLine.Data}");
            _cmdInError = true;

        }
    }
}
