﻿using System;

namespace RunSQL.BatchRunners
{
    public class FinishedEventArgs : EventArgs
    {
        public string Filename { get; set; }
        public bool Success { get; set; }
    }
}
