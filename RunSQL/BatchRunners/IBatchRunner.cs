﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RunSQL.BatchRunners
{
    public interface IBatchRunner
    {                
        void Run(string filename);
        void SetDatabaseConnectionInfo(string ServerConnection, string User, string Password, IEnumerable<string> Databases);

        event EventHandler<InProgressEventArgs> InProgress;
        event EventHandler<FinishedEventArgs> Finished;
    }
}
