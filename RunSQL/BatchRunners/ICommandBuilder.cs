﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RunSQL.BatchRunners
{
    public interface ICommandBuilder
    {
        void SetScriptFilename(string filename);
        string GetCommand();
        string GetCommandArgs();
    }
}
