﻿using System;

namespace RunSQL.BatchRunners
{
    public class InProgressEventArgs : EventArgs
    {
        public string Filename { get; set; }
        public string Database { get; set; }
    }
}
