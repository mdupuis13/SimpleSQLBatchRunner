﻿using System;
using System.Collections.Generic;
using System.Text;
namespace RunSQL.BatchRunners.SQLServer
{
    /// <summary>
    /// Runs a Microsoft SQL Server command using SQLCMD on the commandline.
    /// </summary>
    public class SQLCMDBuilder : ICommandBuilder
    {
        public SQLCMDBuilder(string server, string database)
        {
            this.server = server;
            this.database = database;

            this.credentialArgs = $" -S {server} -d {database}";
        }
    
        /// <summary>
        /// Adds the filename provided to the command's argument list.
        /// </summary>
        /// <param name="filename">filename to be ran by <see cref="GetCommand"/></param>
        public void SetScriptFilename(string filename)
        {
            this.scriptFileArg = $" -i \"{filename}\"";
        }

        /// <summary>
        /// Returns the path to the runnable command.
        /// </summary>
        /// <returns><c>string</c> path to the executable file.</returns>
        public string GetCommand()
        {
            string SqlCmdPath = Environment.GetEnvironmentVariable("SQLCMD_PATH");

            return string.IsNullOrEmpty(SqlCmdPath) ? BaseSqlCmdExe : SqlCmdPath;            
        }

        /// <summary>
        /// Parameters to pass to <see cref="GetCommand"/>
        /// </summary>
        /// <returns><c>string</c> of parameters to pass to command.</returns>
        public string GetCommandArgs()
        {
            return $"{credentialArgs} {scriptFileArg}";
        }

        private string server;
        private string database;
        private const string BaseSqlCmdExe = "SQLCMD.exe ";
        private string credentialArgs = string.Empty;
        private string scriptFileArg = string.Empty;
    }
}
