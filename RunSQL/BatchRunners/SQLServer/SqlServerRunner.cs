﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace RunSQL.BatchRunners.SQLServer
{
    public class SqlServerRunner : IBatchRunner
    {
        public event EventHandler<InProgressEventArgs> InProgress;
        public event EventHandler<FinishedEventArgs> Finished;

        private string server;
        private string user;
        private string password;
        private SQLCMDBuilder cmdBuilder;
        private IEnumerable<string> databases = null;

        private readonly ILogger logger;
        private readonly DOSCommandRunner cmdRunner;

        public SqlServerRunner(ILogger<SqlServerRunner> logger, DOSCommandRunner commandRunner)
        {
            this.logger = logger;
            this.cmdRunner = commandRunner;
        }

        void IBatchRunner.SetDatabaseConnectionInfo(string ServerConnection, string User, string Password, IEnumerable<string> Databases)
        {
            server = ServerConnection;
            user = User;
            password = Password;
            databases = Databases;
        }

        void IBatchRunner.Run(string filename)
        {
            foreach(string db in databases)
            {
                var cmdIsSuccessful = false;

                InProgress(this, new InProgressEventArgs
                {
                    Filename = filename,
                    Database = db
                });

                cmdBuilder = new SQLCMDBuilder(server, db);
                cmdBuilder.SetScriptFilename(filename);

                string cmd = cmdBuilder.GetCommand();
                string cmdargs = cmdBuilder.GetCommandArgs();

                this.logger.LogTrace($"{cmd} {cmdargs}");

                try
                {
                    cmdRunner.RunCommand(cmd, cmdargs);
                    cmdIsSuccessful = true;
                }
                catch (SubCommandException)
                {
                    cmdIsSuccessful = false;
                }
                finally
                {
                    Finished(this, new FinishedEventArgs
                    {
                        Filename = filename,
                        Success = cmdIsSuccessful
                    });
                }
            }
        }
    }
}
