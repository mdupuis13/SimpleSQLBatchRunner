﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RunSQL.BatchRunners
{
    public class SubCommandException :  Exception
    {
        public SubCommandException(string message) : base(message)
        {

        }
    }
}
