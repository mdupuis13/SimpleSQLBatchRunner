﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommandLineParser;
using CommandLineParser.Exceptions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using RunSQL.BatchRunners;
using RunSQL.BatchRunners.SQLServer;

namespace RunSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            var cliOptions = new Options();
            
            var parser = new CommandLineParser.CommandLineParser();
            parser.ExtractArgumentAttributes(cliOptions);

            try 
            {
                parser.ParseCommandLine(args);
                parser.ShowParsedArguments();
            }
            catch (CommandLineException ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                parser.ShowUsage();
                return;
            }    

            var servicesProvider = BuildDi(cliOptions);
            
            ILoggerFactory loggerFactory = servicesProvider.GetRequiredService<ILoggerFactory>();
            ILogger logger = loggerFactory.CreateLogger<Program>();

            logger.LogDebug("Args parsed correctly.");

            if (logger.IsEnabled(LogLevel.Trace))
            { 
                LogArgs(logger, cliOptions);
            }

            /* 
             * Reference to SQL Server runner hardcoded as we support only SQL Server for now.
             * In the future, we will have to select sqlRunner based on a commandline argument so
             * we can support other databases (MySQL/MariaDB, PostgreSQL and Oracle, etc).
             * */
            IBatchRunner sqlRunner = servicesProvider.GetService<IBatchRunner>();
            sqlRunner.SetDatabaseConnectionInfo(cliOptions.Server, cliOptions.User, cliOptions.Password, cliOptions.Databases);

            var runner = servicesProvider.GetService<ScriptRunner>();
            runner.SetOptions(cliOptions);
            runner.DoAction(sqlRunner);
        }

        private static void LogArgs(ILogger logger, Options options)
        {
            logger.LogTrace($"server: {options.Server}");
            foreach (string database in options.Databases)
            {
                logger.LogTrace($"database: {database}");
            }

            if (options.User != null)
            {
                logger.LogTrace($"user: {options.User}  password: {options.Password}");
            }
        }

        private static IServiceProvider BuildDi(Options options)
        {
            var service = new ServiceCollection()
                .AddTransient<IBatchRunner, SqlServerRunner>()
                .AddTransient<ScriptRunner>()
                .AddTransient<ScriptDiscovery>()
                .AddTransient<ICommandBuilder, SQLCMDBuilder>()
                .AddSingleton<DOSCommandRunner>()
                // logging
                .AddSingleton<ILoggerFactory, LoggerFactory>()
                .AddSingleton(typeof(ILogger<>), typeof(Logger<>));

            // set minimum logging level
            if (options.Verbose)
            { 
                service.AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Debug));
            }            
            else if (options.VeryVerbose)
            { 
                service.AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Trace));
            }
            else
            { 
                service.AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Information));
            }

            var serviceProvider = service.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            NLog.LogManager.LoadConfiguration("NLog.config");

            return serviceProvider;
        }
    }
}
