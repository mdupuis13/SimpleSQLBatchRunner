﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace RunSQL
{
    public class ScriptDiscovery
    {
        private readonly ILogger logger;
        private const string fileFilter = "*.sql";

        public ScriptDiscovery(ILogger<ScriptDiscovery> logger)
        {
            this.logger = logger;
        }
  
        /// <summary>
        /// Returns a list of SQL script files to run.
        /// <para>Searches <c>path</c> recursively for all *.sql files. Recurses into the first level only.</para>
        /// <para>Orders the file in alphabetical order.</para>
        /// </summary>
        /// <param name="path">Complete path to the SQL script files.</param>
        /// <returns><c>IEnumerable</c> of <see cref="FileInfo"/> files.</returns>
        public IEnumerable<FileInfo> GetListOfScriptsToRun(string path)
        {
            List<string> fileList = new List<string>();

            if (Directory.Exists(path))
            {
                // add this folder's files
                fileList.AddRange(Directory.GetFiles(path, fileFilter));
                
                // add files from sub-folders
                foreach (var dir in Directory.GetDirectories(path))
                {
                    var files = this.GetListOfScriptsToRun(dir);
                    fileList.AddRange(files.Select(x => x.FullName));
                }
            }

            logger.LogInformation($"Found {fileList.Count()} files");

            if (fileList.Count() > 0 && logger.IsEnabled(LogLevel.Debug))
            {
                logger.LogDebug($"List of files found:");
                foreach (var file in fileList)
                {
                    logger.LogDebug($"\t{file}");
                }
            }

            return fileList
                    .Select(file => new FileInfo(file))
                    .OrderBy(f => f.FullName);
        }
    }
}
