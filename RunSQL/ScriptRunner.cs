﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using RunSQL.BatchRunners;
using RunSQL.BatchRunners.SQLServer;

namespace RunSQL
{
    public class ScriptRunner
    {
        private readonly ILogger logger;
        private readonly ScriptDiscovery scriptDiscovery;
        private Options options;

        public ScriptRunner(ILogger<ScriptRunner> logger, ScriptDiscovery sd)
        {
            this.logger = logger;
            this.scriptDiscovery = sd;
        }

        public void DoAction(IBatchRunner batchRunner)
        {
            batchRunner.InProgress += BatchRunner_InProgress;
            batchRunner.Finished += BatchRunner_Finished;

            string scriptsBasePath = GetScriptBasePath(options.ScriptsFolder);

            foreach (var file in scriptDiscovery.GetListOfScriptsToRun(scriptsBasePath))
            {
                batchRunner.Run(file.FullName);
            }
        }

        public string GetScriptBasePath(string optionsFolder)
        {
            string retval = optionsFolder;

            if (!string.IsNullOrEmpty(optionsFolder))
            {
                if (System.IO.Path.IsPathRooted(optionsFolder))
                {
                    retval = optionsFolder;
                }
                else
                {
                    retval = System.IO.Path.Combine(AppContext.BaseDirectory, optionsFolder);
                }
            }

            return retval;
        }

        public void SetOptions(Options options)
        {
            this.options = options;
        }

        private void BatchRunner_InProgress(object sender, InProgressEventArgs e)
        {
            logger.LogDebug($"In progress: {e.Filename}");
        }

        private void BatchRunner_Finished(object sender, FinishedEventArgs e)
        {
            logger.LogInformation($"Finished: {e.Filename}");
        }
    }
}
