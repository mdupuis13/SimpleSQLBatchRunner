﻿using System;
using System.Collections.Generic;
using System.Text;using CommandLineParser.Arguments;
using CommandLineParser.Validation;

namespace RunSQL
{
    [ArgumentGroupCertification("u,p", EArgumentGroupCondition.AllOrNoneUsed)]
    public class Options
    {
        [ValueArgument(typeof(string), 'f', "folder",
            AllowMultiple = false, 
            Description = "Full path to directory containing SQL script files (*.sql).")]
        public string ScriptsFolder { get; set; }

        [ValueArgument(typeof(string), 's', "server",
            Optional = false,
            AllowMultiple = false, 
            Description = "SQL server connectionstring.")]
        public string Server { get; set; }

        [ValueArgument(typeof(string), 'd', "database",
            Optional = false,
            AllowMultiple = true,
            Description = "List of databases to apply scripts to.")]
        public List<string> Databases = new List<string>();

        [ValueArgument(typeof(string), 'u', "user",
            AllowMultiple = false, 
            Description = "Username for SQL Server. If not provided, uses Windows Authentication.")]
        public string User { get; set; }

        [ValueArgument(typeof(string), 'p', "pwd",
            AllowMultiple = false, 
            Description = "Password for username. Ignored if username is not provided.")]
        public string Password { get; set; }

        [SwitchArgument('v', false, 
            AllowMultiple = false, 
            Description = "Verbose output (Debug level).")]
        public bool Verbose { get; set; }

        [SwitchArgument("vv", false, 
            AllowMultiple = false, 
            Description = "Very verbose output (Trace level).")]
        public bool VeryVerbose { get; set; }
    }
}
